fbzx (4.8.0-Debian1) sid; urgency=low

  * Fixed ULAPlus mode

 -- Sergio Costas Rodriguez <raster@rastersoft.com>  Sun, 30 May 2021 23:55:00 +0200

fbzx (4.7.0-Debian1) sid; urgency=low

  * Fixed inverted left-right keys in Sinclair joystick emulation

 -- Sergio Costas Rodriguez <raster@rastersoft.com>  Thu, 15 Apr 2021 23:55:00 +0200

fbzx (4.6.0-Debian1) sid; urgency=low

  * Fixed keyboard lag when a key is kept pressed (due to autorrepeat)

 -- Sergio Costas Rodriguez <raster@rastersoft.com>  Tue, 30 Mar 2021 23:55:00 +0200

fbzx (4.5.0-Debian1) sid; urgency=low

  * Allows to show the FPS in a game
  * Fixed making Z80 Snapshots

 -- Sergio Costas Rodriguez <raster@rastersoft.com>  Sun, 21 Mar 2021 23:55:00 +0200

fbzx (4.4.0-Debian1) sid; urgency=low

  * RETI doesn't enable interrupts. Fixed.
  * Removed OSS support
  * Use 16-bit for sound
  * Limit the volume output to avoid interferences in PulseAudio
  * Add MIC emulation
  * Added sound filtering to enhance sound quality

 -- Sergio Costas Rodriguez <raster@rastersoft.com>  Fri, 12 Feb 2021 23:55:00 +0200

fbzx (4.3.1-Debian1) sid; urgency=low

  * REALLY set ALSA as default audio output

 -- Sergio Costas Rodriguez <raster@rastersoft.com>  Sat, 30 Jan 2021 23:55:00 +0200

fbzx (4.3.0-Debian1) sid; urgency=low

  * Fixed 48K Z80 snaps
  * Set ALSA as default audio output

 -- Sergio Costas Rodriguez <raster@rastersoft.com>  Fri, 29 Jan 2021 23:55:00 +0200

fbzx (4.2.0-Debian1) sid; urgency=low

  * Fixed RETI

 -- Sergio Costas Rodriguez <raster@rastersoft.com>  Sun, 17 Jan 2021 23:55:00 +0200

fbzx (4.1.0-Debian1) sid; urgency=low

  * Fixed BRIGHT colors

 -- Sergio Costas Rodriguez <raster@rastersoft.com>  Mon, 11 Jan 2021 23:55:00 +0200

fbzx (4.0.0-Debian1) sid; urgency=low

  * Now fullscreen works fine with Wayland (ported to SDL2)

 -- Sergio Costas Rodriguez <raster@rastersoft.com>  Sat, 11 Aug 2018 23:55:00 +0200

fbzx (3.9.1-Debian1) sid; urgency=low

  * Removed unused printfs

 -- Sergio Costas Rodriguez <raster@rastersoft.com>  Tue, 01 May 2018 23:55:00 +0200

fbzx (3.9.0-Debian1) sid; urgency=low

  * Added letter and number key handling for file selector (thanks to
Adrian Castravete)

 -- Sergio Costas Rodriguez <raster@rastersoft.com>  Tue, 01 May 2018 23:50:00 +0200

fbzx (3.8.0-Debian1) sid; urgency=low

  * Updated the Z80 emulator and the screen emulation, which now is so
precise that can run flawlessly the SHOCK demo both in 48K and +3 mode
  * Removed the code that removed the DC in the audio output, because the
sound was really ugly

 -- Sergio Costas Rodriguez <raster@rastersoft.com>  Mon, 06 Mar 2017 23:55:00 +0200

fbzx (3.7.0-Debian1) sid; urgency=low

  * Now the interrupt line is kept low exactly 32tstates
  * Now the EI as prefix works fine

 -- Sergio Costas Rodriguez <raster@rastersoft.com>  Sat, 04 Mar 2017 23:55:00 +0200

fbzx (3.6.0-Debian1) sid; urgency=low

  * Fixed timing bug

 -- Sergio Costas Rodriguez <raster@rastersoft.com>  Tue, 14 Feb 2017 23:55:00 +0200

fbzx (3.5.0-Debian1) sid; urgency=low

  * Even better adjusted the memory contention

 -- Sergio Costas Rodriguez <raster@rastersoft.com>  Mon, 13 Feb 2017 23:55:00 +0200

fbzx (3.4.0-Debian1) sid; urgency=low

  * Slightly better adjusted the memory contention

 -- Sergio Costas Rodriguez <raster@rastersoft.com>  Mon, 13 Feb 2017 23:50:00 +0200

fbzx (3.3.0-Debian1) sid; urgency=low

  * Fixed the Fast Load
  * Now the tape is paused automatically when it reachs the end

 -- Sergio Costas Rodriguez <raster@rastersoft.com>  Mon, 13 Feb 2017 23:45:00 +0200

fbzx (3.2.0-Debian1) sid; urgency=low

  * Better memory contention
  * Fixed pause block in TZX files

 -- Sergio Costas Rodriguez <raster@rastersoft.com>  Sun, 12 Feb 2017 23:55:00 +0200

fbzx (3.1.0-Debian1) sid; urgency=low

  * Added Kempston Mouse emulation
  * Added block 30 support for TZX files

 -- Sergio Costas Rodriguez <raster@rastersoft.com>  Fri, 16 Dec 2016 23:55:00 +0200

fbzx (3.0.0-Debian1) sid; urgency=low

  * Now allows to run in TURBO mode when the tape is playing, and return
to NORMAL mode when the tape is paused
  * Sorts the files and folders alphabetically
  * Allows to use fast-load with some parts of TZX files
  * Allows to save data both in TAP and TZX files
  * Allows to do FAST-SAVE in +3 mode
  * Allows to create TZX files
  * New fonts for menus
  * Fixed an stupid bug in the FAST LD_BYTES code that avoided it to work
as expected
  * Supports new TZX blocks, like "pause if 48K mode"
  * Refactorized in C++

 -- Sergio Costas Rodriguez <raster@rastersoft.com>  Wed, 01 Apr 2015 23:55:00 +0200

fbzx (2.11.0-Debian1) sid; urgency=low

  * Allows to play and pause the tape even with the FAST LOAD mode enabled
  * Adds some noise in the tape bit to ensure that the user knows when a
program is waiting to load from tape

 -- Sergio Costas Rodriguez <raster@rastersoft.com>  Tue, 31 Mar 2015 23:55:00 +0200

fbzx (2.5.0-Debian1) sid; urgency=low

  * Allows to set POKEs
  * Emulates B&W sets
  * Allows to load and save SCR snapshots

 -- Sergio Costas Rodriguez <raster@rastersoft.com>  Thu, 07 Apr 2011 23:55:00 +0200

